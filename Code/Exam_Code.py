import pandas as pd     # import libraries
import os

#------------------------------- PART_1 -------------------------------
##### Q1: Reading Data
input1 = "../Data/part1.csv"     # Specify the csv file path of part1 and part2
input2 = "../Data/part3.csv"     # Specify the csv file path of part3

MyDataFrame = pd.read_csv(os.path.abspath(input1), sep=",")     # Read in data of part1.csv


##### Q2: Plots of costs distribution PER COURSE
### Version 1 : per COURSES
import matplotlib.pyplot as plt     # import libraries

Course1_Data = MyDataFrame.loc[:,'FIRST_COURSE'].values     # Get the courses data from columns names to ease the work with vectors
Course2_Data = MyDataFrame.loc[:,'SECOND_COURSE'].values
Course3_Data = MyDataFrame.loc[:,'THIRD_COURSE'].values

Courses_Data = {'1st Course': {'data': Course1_Data,     #  Create a dictionnary to make the plots with the corresponding details
                               'color': 'g',
                               'title': '1st Course',
                               'row':0,
                               'column': 0},
                '2nd Course': {'data': Course2_Data,
                               'color': 'b',
                               'title': '2nd Course',
                               'row': 0,
                               'column': 1
                               },
                '3rd Course': {'data': Course3_Data,
                               'color': 'r',
                               'title': '3rd Course',
                               'row': 0,
                               'column': 2
                               },
                }

kwargs = dict(alpha=0.8, bins=10)     # Properties of colour and bar sizes for the different plots
fig, axes = plt.subplots(1, len(Courses_Data), figsize=(20,9))     # Prepare the interface to plot properly our graphs

for key in Courses_Data:     # Loop through the column names to plot for each course the distribution of costs with the necessary details
    axes[Courses_Data[key]['column']].hist(Courses_Data[key]['data'], **kwargs, color=Courses_Data[key]['color'], label=Courses_Data[key]['title'])
    axes[Courses_Data[key]['column']].set_xlabel('Values')
    axes[Courses_Data[key]['column']].set_ylabel('Frequency')
    axes[Courses_Data[key]['column']].legend()

plt.suptitle('Courses costs distribution histograms')     # Set the title for the plot

plt.savefig(os.path.abspath("../Result/part1-1.png"))    # Save the output in our project file
plt.show()     # Show the plot
plt.close()     # Close the figure just after you saving it to be able to save properly the upcoming figures

### Version 2 : Plots of costs distribution PER TIMES (Lunch, Dinner)
Lunch_Costs = MyDataFrame.loc[MyDataFrame['TIME'] == 'LUNCH',['FIRST_COURSE','SECOND_COURSE','THIRD_COURSE']].sum(axis=1)     # Get total Lunch costs (All courses) for all Lunch rows in the data
Dinner_Costs = MyDataFrame.loc[MyDataFrame['TIME'] == 'DINNER',['FIRST_COURSE','SECOND_COURSE','THIRD_COURSE']].sum(axis=1)     # Get total Dinner costs (All courses) for all Dinner rows in the data

Time_Costs_Data = {'Lunch': {'data': Lunch_Costs,     #  Create a dictionnary to make the plots with the corresponding details
                               'color': 'g',
                               'title': 'Lunch time',
                               'row':0,
                               'column': 0},
                'Dinner': {'data': Dinner_Costs,
                               'color': 'b',
                               'title': 'Dinner time',
                               'row': 0,
                               'column': 1
                           },
                   }

kwargs = dict(alpha=0.8, bins=10)     # Properties of colour and bar sizes for the different plots
fig, axes = plt.subplots(1, len(Time_Costs_Data), figsize=(20,9))     # Prepare the interface to plot properly our graphs

for key in Time_Costs_Data:     # Loop through the column names to plot for each time the distribution of total costs with the necessary details
    axes[Time_Costs_Data[key]['column']].hist(Time_Costs_Data[key]['data'], **kwargs, color=Time_Costs_Data[key]['color'], label=Time_Costs_Data[key]['title'])
    axes[Time_Costs_Data[key]['column']].set_xlabel('Values')
    axes[Time_Costs_Data[key]['column']].set_ylabel('Frequency')
    axes[Time_Costs_Data[key]['column']].legend()

plt.suptitle('Time costs distribution histograms')     # Set the title for the plot

plt.savefig(os.path.abspath("../Result/part1-2.png"))    # Save the output in our project file
plt.show()     # Show the plot
plt.close()     # Close the figure just after you saving it to be able to save properly the upcoming figures

##### Q3: Plots of costs distribution per course
plt.figure(1, figsize=(10,8))     # Prepare the interface to plot the graph with a convenient size

Courses_Average_Costs = MyDataFrame[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]].mean()     # Get the cost average for each course
axis_labels = ["1st course","2nd course","3rd course"]     # Set axis labels corresponding to the # courses
plt.bar(axis_labels,Courses_Average_Costs,color=['green','blue','red'])    # Bar plot of the average costs with different colors

plt.title('Bar plot of average costs per course')     # Give the titles for # axis
plt.ylabel("Average cost")
plt.xlabel("Course type")

plt.savefig(os.path.abspath("../Result/part1-3.png"))    # Save the output in our project file
plt.show()     # Show the plot
plt.close()     # Close the figure just after you saving it to be able to save properly the upcoming figures

##### 04 + Q5 : Calculate cost of  drinks and adding new columns
import numpy as np     # import libraries

# Set up conditions for filtering each course into different price ranges
# Start with course 1 and then do the same for the others
conditions = [
    (MyDataFrame['FIRST_COURSE'] >= 3) & (MyDataFrame['FIRST_COURSE'] < 15),
    (MyDataFrame['FIRST_COURSE'] >= 15) & (MyDataFrame['FIRST_COURSE'] < 20),
    (MyDataFrame['FIRST_COURSE'] >= 20)]
# Choices that will be then assigned for each range
choices = [
    (MyDataFrame['FIRST_COURSE'] - 3),
    (MyDataFrame['FIRST_COURSE'] - 15),
    (MyDataFrame['FIRST_COURSE'] - 20)]
# New column to store the calculated data
MyDataFrame['DRINKS_FC'] = np.select(conditions, choices, default=0)
# For the name of the food, we just change the cost of drinks to the name of the dish
choices = ['SOUP', 'TOMATO-MOZARELLA', 'OYSTERS']
MyDataFrame['FOOD_FC'] = np.select(conditions, choices, default=0)

#carry out the same step for the other 2 courses
conditions = [
    (MyDataFrame['SECOND_COURSE'] >= 9) & (MyDataFrame['SECOND_COURSE'] < 20),
    (MyDataFrame['SECOND_COURSE'] >= 20) & (MyDataFrame['SECOND_COURSE'] < 25),
    (MyDataFrame['SECOND_COURSE'] >= 25) & (MyDataFrame['SECOND_COURSE'] < 40),
    (MyDataFrame['SECOND_COURSE'] >= 40)]
choices = [
    (MyDataFrame['SECOND_COURSE'] - 9),
    (MyDataFrame['SECOND_COURSE'] - 20),
    (MyDataFrame['SECOND_COURSE'] - 25),
    (MyDataFrame['SECOND_COURSE'] - 40)]
MyDataFrame['DRINKS_SC'] = np.select(conditions, choices, default=0)
choices = ['SALAD', 'SPAGHETTI', 'STEAK', 'LOBSTER']
MyDataFrame['FOOD_SC'] = np.select(conditions, choices, default=0)

conditions = [
    (MyDataFrame['THIRD_COURSE'] >= 10) & (MyDataFrame['THIRD_COURSE'] < 15),
    (MyDataFrame['THIRD_COURSE'] >= 15)]
choices = [
    (MyDataFrame['THIRD_COURSE'] - 10),
    (MyDataFrame['THIRD_COURSE'] - 15)]
MyDataFrame['DRINKS_TC'] = np.select(conditions, choices, default=0)
choices = ['PIE', 'ICE CREAM']
MyDataFrame['FOOD_TC'] = np.select(conditions, choices, default=0)

#Final Result
print(MyDataFrame)
Drinks_Costs = MyDataFrame     # Save this dataframe with this name to use it afterthat



#------------------------------- PART_2 -------------------------------
##### Q1: Clustering using kmeans data on cost per course
from mpl_toolkits.mplot3d import Axes3D     # Import necessary libraries
from sklearn.cluster import KMeans

MyDataFrame_to_Cluster = pd.read_csv(os.path.abspath(input1), sep=",")     # Return to our original data

np.random.seed(5)     # Starting value in generating random numbers and saving the state of randomness

X = MyDataFrame_to_Cluster[['FIRST_COURSE','SECOND_COURSE','THIRD_COURSE']]     # The data to be trained / learnt is the curses costs columns from the original dataset

name = 'k_means_iris_4'
est = KMeans(n_clusters=4)     # Assuming we have 4 clusters to do the clustering
est.fit(X)     # Train the model using .fit() method

y = est.labels_     # y here is our target output, It's the list of clusters for which our records belong. It's the same as y in the iris model we did
print(set(y))     # Indeed, when printing the set of y, we found 4 clusters which means that the model has already 4 clusters/groups after training the data
# REMARK : we have 4 clusters BUT we haven't identified yet our restaurants groups, i.e. we don't know who is the grp of cluster num 0, etc...

# Printing these two lines is extremely important to know what is our new trained data
print(X)     # [36500 rows x 3 columns] It's a list of all rows with our courses columns
print(y)     # [0 0 1 ... 3 1 3] gives the cluster group number for each record in the dataset

fignum = 1     # Plotting details
titles = ['4 clusters']

X = X.to_numpy()     # Change X to an array. REMARK : the plotting part below works only when running this line (it marks error in X[:, 0], X[:, 1])

fig = plt.figure(fignum, figsize=(8, 7))
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
ax.scatter(X[:, 0], X[:, 1], X[:, 2],
           c=y.astype(np.float), edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
ax.set_xlabel('Dish1')     # We tried to set these features as "physical apperances" to find (Like length and width with iris)
ax.set_ylabel('Dish2')
ax.set_zlabel('Dish3')
ax.set_title(titles[fignum - 1])
ax.dist = 12
fignum = fignum + 1     # ====> The plot confirms what we said but we still don't know our restaurant groups. We'll find them out in the next lines...

plt.savefig(os.path.abspath("../Result/part2-1.png"))    # Save the output in our project file
plt.close()

##### Q2 + Q3 + Q4: Identification of groups + Adding labels to the restaurant truth + Looking for specific caracteristics per group
Transposed_Clustered_Data = np.append(X,y.reshape(36500,1),axis=1)     # Transform our trained data into a Dataframe to ease the identification of each group
Transposed_Clustered_Data_to_dataframe = pd.DataFrame(Transposed_Clustered_Data.T,index=['Dish1','Dish2','Dish3','Cluster_Group'])
Clustered_Data = Transposed_Clustered_Data_to_dataframe.T     # We did the transpose because the column names were indexes. Now the data is well prepared

## Determine the business group by summing the dishes costs and divide by the total number of clients per cluster. The highest value of average will
# correspond to the business clients
Cluster_0 = (Clustered_Data[['Dish1','Dish2','Dish3']].sum(axis=1).where(Clustered_Data['Cluster_Group'] == 0,0)).sum()     # 456908
Cluster_0_Average = Cluster_0 / len(Clustered_Data.loc[Clustered_Data['Cluster_Group'] == 0])     # 28.78
Cluster_1 = (Clustered_Data[['Dish1','Dish2','Dish3']].sum(axis=1).where(Clustered_Data['Cluster_Group'] == 1,0)).sum()     # 284224
Cluster_1_Average = Cluster_1 / len(Clustered_Data.loc[Clustered_Data['Cluster_Group'] == 1])     # 52.06
Cluster_2 = (Clustered_Data[['Dish1','Dish2','Dish3']].sum(axis=1).where(Clustered_Data['Cluster_Group'] == 2,0)).sum()     # 444836
Cluster_2_Average = Cluster_2 / len(Clustered_Data.loc[Clustered_Data['Cluster_Group'] == 2])     # 61.11
Cluster_3 = (Clustered_Data[['Dish1','Dish2','Dish3']].sum(axis=1).where(Clustered_Data['Cluster_Group'] == 3,0)).sum()     # 128113
Cluster_3_Average = Cluster_3 / len(Clustered_Data.loc[Clustered_Data['Cluster_Group'] == 3])     # 16.23
# ====> The Cluster_2 corresponds to the business clients as it has the highest value

## Determine the retirement group by searching for the group that has the combination of 3 courses with a pie as an end dish. # The highest number will
# correspond to the retirement clients. We'll get help from MyDataFrame of Part 1
pd.Series(MyDataFrame['CLIENT_ID']).is_unique     # True : After checking that the ID column has only different value, let's do an inner join with the
# Clustered_Data to the Right part of MyDataFrame with which we ended the part 1

Clustered_Data.insert(loc=0, column='CLIENT_ID', value=MyDataFrame['CLIENT_ID'])     # Add the column of clients ID by which we'll do the join

Clustered_Data_Merged = pd.merge(Clustered_Data,MyDataFrame,on='CLIENT_ID')     # Do the join
Clustered_Data_Merged = Clustered_Data_Merged.iloc[:, [0,1,2,3,4,10,12,14]]     # Select only the columns of ID, dishes, clusters groups and FOODS gotten in Part1

# Let's identify now
Cluster_To_Test = (Clustered_Data_Merged.loc[(Clustered_Data_Merged['FOOD_FC'] != '0') & (Clustered_Data_Merged['FOOD_SC'] != '0') &
                  (Clustered_Data_Merged['FOOD_TC'] == 'PIE')])     # Select the rows where we have the criterias mentionned just above

len(Cluster_To_Test.loc[Cluster_To_Test['Cluster_Group'] == 0])     # 1502
len(Cluster_To_Test.loc[Cluster_To_Test['Cluster_Group'] == 1])     # 4653
len(Cluster_To_Test.loc[Cluster_To_Test['Cluster_Group'] == 3])     # 226
# ====> The Cluster nbr 1 corresponds to the retirement clients as it has the highest value

## Determine the healthy group by simply searching for the group that has the highest count in records of a soup for a starter. The highest one will be
# the healthy clients because normal clients rarely take a starter unlike healthy who do have it instead of salad (the frequency of SOUP is likely to be
# higher for them
Cluster_To_Test = Clustered_Data_Merged.loc[(Clustered_Data_Merged['FOOD_FC'] == 'SOUP')]
len(Cluster_To_Test.loc[Cluster_To_Test['Cluster_Group'] == 0])     # 1684
len(Cluster_To_Test.loc[Cluster_To_Test['Cluster_Group'] == 3])     # 7592
# ====> The Cluster nbr 3 corresponds to the healthy clients as it has the highest value. Indeed, Cluster nbr 2 is the normal ones who came onetime

# NOW PLOT the RESTAURANT TRUTH
fig = plt.figure(fignum, figsize=(8, 7))
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)

for name, label in [('Business', 2),
                    ('Retirement', 1),
                    ('Onetime', 0),
                    ('Healthy', 3)]:
    ax.text3D(X[y == label, 0].mean(),
              X[y == label, 1].mean(),
              X[y == label, 2].mean() + 2, name,
              horizontalalignment='center',
              bbox=dict(alpha=.2, edgecolor='w', facecolor='w'))
# Reorder the labels to have colors matching the cluster results
y = np.choose(y, [0, 1, 2, 3]).astype(np.float)
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y, edgecolor='k')

ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
ax.set_xlabel('Dish1')
ax.set_ylabel('Dish2')
ax.set_zlabel('Dish3')
ax.set_title('Restaurant Customers')
ax.dist = 12

plt.savefig(os.path.abspath("../Result/part2-2.png"))    # Save the output in our project file
fig.show()
plt.close()     # Close the figure just after you saving it to be able to save properly the upcoming figures


#------------------------------- PART_3 -------------------------------
TruthResult = pd.read_csv(os.path.abspath("../Data/part3.csv"), sep=",")      # Import the actual data
Prediction = Clustered_Data_Merged     # Set the found dataframe in last P2 as the prediction to compare with actual

Prediction.loc[Prediction['Cluster_Group']==0, 'Cluster_Group'] = 'Onetime'      # Keep only the records from the same cluster group
Prediction.loc[Prediction['Cluster_Group']==1, 'Cluster_Group'] = 'Retirement'
Prediction.loc[Prediction['Cluster_Group']==2, 'Cluster_Group'] = 'Business'
Prediction.loc[Prediction['Cluster_Group']==3, 'Cluster_Group'] = 'Healthy'

MyDataFrame = pd.merge(Prediction,TruthResult,on='CLIENT_ID')      # Join two dataframes based on clients ID

#Q1
MyDataFrame['Compare'] = np.where(MyDataFrame['Cluster_Group']==MyDataFrame['CLIENT_TYPE'],'yes', 'no')     # Compute the prediction precision
Accuracy = len(MyDataFrame.loc[MyDataFrame['Compare']=='yes']) / len(MyDataFrame['Compare']) * 100
print('Our prediction has an accuracy rate of %s percent' %(round(Accuracy,2)))

#Q2
bsclient = len(MyDataFrame.loc[MyDataFrame['Cluster_Group']=='Business']) / len(MyDataFrame['Cluster_Group']) * 100    # Frequency calculation for each data type
otclient = len(MyDataFrame.loc[MyDataFrame['Cluster_Group']=='Onetime']) / len(MyDataFrame['Cluster_Group']) * 100
hclient = len(MyDataFrame.loc[MyDataFrame['Cluster_Group']=='Healthy']) / len(MyDataFrame['Cluster_Group']) * 100
rtclient = len(MyDataFrame.loc[MyDataFrame['Cluster_Group']=='Retirement']) / len(MyDataFrame['Cluster_Group']) * 100
#https://matplotlib.org/3.1.1/gallery/pie_and_polar_charts/pie_features.html#sphx-glr-gallery-pie-and-polar-charts-pie-features-py
labels = 'Business', 'Onetime', 'Healthy', 'Retirement'    # Pie chart plotting with clients labels
sizes = [bsclient, otclient, hclient, rtclient]     # List of clients type frequency presence in the data
explode = (0, 0, 0, 0)
fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
plt.savefig(os.path.abspath("../Result/part3-1.png"))
plt.show()

#Q3
BusinessDF = MyDataFrame.drop(MyDataFrame[MyDataFrame['Cluster_Group']!='Business'].index)      # Keep ONLY Bsc clients
pctBFC = len(BusinessDF.loc[BusinessDF['FOOD_FC'] != '0']) / len(BusinessDF['FOOD_FC']) * 100   # % of Food taking for Bsc Clients among all Bsc Clients
pctBSC = len(BusinessDF.loc[BusinessDF['FOOD_SC'] != '0']) / len(BusinessDF['FOOD_SC']) * 100
pctBTC = len(BusinessDF.loc[BusinessDF['FOOD_TC'] != '0']) / len(BusinessDF['FOOD_TC']) * 100
print('Business clients have %s percent chance to order first course, %s percent chance to order second course and %s percent chance to order third course' %(round(pctBFC,2),round(pctBSC,2),round(pctBTC,2)))

OnetimeDF = MyDataFrame.drop(MyDataFrame[MyDataFrame['Cluster_Group']!='Onetime'].index)        # Same method as above
pctOFC = len(OnetimeDF.loc[OnetimeDF['FOOD_FC'] != '0']) / len(OnetimeDF['FOOD_FC']) * 100      # Same method as above
pctOSC = len(OnetimeDF.loc[OnetimeDF['FOOD_SC'] != '0']) / len(OnetimeDF['FOOD_SC']) * 100
pctOTC = len(OnetimeDF.loc[OnetimeDF['FOOD_TC'] != '0']) / len(OnetimeDF['FOOD_TC']) * 100
print('Onetime clients have %s percent chance to order first course, %s percent chance to order second course and %s percent chance to order third course' %(round(pctOFC,2),round(pctOSC,2),round(pctOTC,2)))

RetirementDF = MyDataFrame.drop(MyDataFrame[MyDataFrame['Cluster_Group']!='Retirement'].index)        # //
pctRFC = len(RetirementDF.loc[RetirementDF['FOOD_FC'] != '0']) / len(RetirementDF['FOOD_FC']) * 100   # //
pctRSC = len(RetirementDF.loc[RetirementDF['FOOD_SC'] != '0']) / len(RetirementDF['FOOD_SC']) * 100
pctRTC = len(RetirementDF.loc[RetirementDF['FOOD_TC'] != '0']) / len(RetirementDF['FOOD_TC']) * 100
print('Retirement clients have %s percent chance to order first course, %s percent chance to order second course and %s percent chance to order third course' %(round(pctRFC,2),round(pctRSC,2),round(pctRTC,2)))

HealthyDF = MyDataFrame.drop(MyDataFrame[MyDataFrame['Cluster_Group']!='Healthy'].index)     # //
pctHFC = len(HealthyDF.loc[HealthyDF['FOOD_FC'] != '0']) / len(HealthyDF['FOOD_FC']) * 100   # //
pctHSC = len(HealthyDF.loc[HealthyDF['FOOD_SC'] != '0']) / len(HealthyDF['FOOD_SC']) * 100
pctHTC = len(HealthyDF.loc[HealthyDF['FOOD_TC'] != '0']) / len(HealthyDF['FOOD_TC']) * 100
print('Healthy clients have %s percent chance to order first course, %s percent chance to order second course and %s percent chance to order third course' %(round(pctHFC,2),round(pctHSC,2),round(pctHTC,2)))

#Q4:
#First course
for j in ('SOUP', 'TOMATO-MOZARELLA', 'OYSTERS'):      # Loop through foods and determine each % of dish presence for each type of course food in each type of client
    y = len(BusinessDF.loc[BusinessDF['FOOD_FC']==j]) / len(BusinessDF['FOOD_FC']) * 100
    print('For Business clients, there is %s percent that they choose %s'%(round(y,2),j))      # Print the result

for j in ('SOUP', 'TOMATO-MOZARELLA', 'OYSTERS'):
    y = len(OnetimeDF.loc[OnetimeDF['FOOD_FC']==j]) / len(OnetimeDF['FOOD_FC']) * 100
    print('For Onetime clients, there is %s percent that they choose %s'%(round(y,2),j))

for j in ('SOUP', 'TOMATO-MOZARELLA', 'OYSTERS'):
    y = len(HealthyDF.loc[HealthyDF['FOOD_FC']==j]) / len(HealthyDF['FOOD_FC']) * 100
    print('For Healthy clients, there is %s percent that they choose %s'%(round(y,2),j))

for j in ('SOUP', 'TOMATO-MOZARELLA', 'OYSTERS'):
    y = len(RetirementDF.loc[RetirementDF['FOOD_FC']==j]) / len(RetirementDF['FOOD_FC']) * 100
    print('For Retirement clients, there is %s percent that they choose %s'%(round(y,2),j))
#Second course
for j in ('SALAD', 'SPAGHETTI', 'STEAK', 'LOBSTER'):
    y = len(BusinessDF.loc[BusinessDF['FOOD_SC'] == j]) / len(BusinessDF['FOOD_SC']) * 100
    print('For Business clients, there is %s percent that they choose %s' % (round(y, 2), j))

for j in ('SALAD', 'SPAGHETTI', 'STEAK', 'LOBSTER'):
    y = len(OnetimeDF.loc[OnetimeDF['FOOD_SC'] == j]) / len(OnetimeDF['FOOD_SC']) * 100
    print('For Onetime clients, there is %s percent that they choose %s' % (round(y, 2), j))

for j in ('SALAD', 'SPAGHETTI', 'STEAK', 'LOBSTER'):
    y = len(HealthyDF.loc[HealthyDF['FOOD_SC'] == j]) / len(HealthyDF['FOOD_SC']) * 100
    print('For Healthy clients, there is %s percent that they choose %s' % (round(y, 2), j))

for j in ('SALAD', 'SPAGHETTI', 'STEAK', 'LOBSTER'):
    y = len(RetirementDF.loc[RetirementDF['FOOD_SC'] == j]) / len(RetirementDF['FOOD_SC']) * 100
    print('For Retirement clients, there is %s percent that they choose %s' % (round(y, 2), j))
#Third course
for j in ('PIE', 'ICE CREAM'):
    y = len(BusinessDF.loc[BusinessDF['FOOD_TC'] == j]) / len(BusinessDF['FOOD_TC']) * 100
    print('For Business clients, there is %s percent that they choose %s' % (round(y, 2), j))

for j in ('PIE', 'ICE CREAM'):
    y = len(OnetimeDF.loc[OnetimeDF['FOOD_TC'] == j]) / len(OnetimeDF['FOOD_TC']) * 100
    print('For Onetime clients, there is %s percent that they choose %s' % (round(y, 2), j))

# Q5:
# Code to get the downer part : we made it independent with the downer part. It's easier to distinguish : the output of this is in comments just below
c=0
persondict = {'HealthyDF':HealthyDF,'RetirementDF':RetirementDF,'BusinessDF':BusinessDF,'OnetimeDF':OnetimeDF}     # The key data of each group
for persondf in persondict.keys():     # Loop through groups
    for course in ['FOOD_FC','FOOD_SC','FOOD_TC']:    # Loop through courses for each group
        persondict[persondf] = persondict[persondf].drop(persondict[persondf][persondict[persondf][course] == '0'].index)     # Keep only Clients who consumed dishes
        distribution = list()    # Prepare an empty list to fill it with the data distribution
        if len(persondict[persondf][course]) == 0:     # Error handling to move on when the length is null
            continue
        for dish in persondict[persondf][course].unique():      # Go through unique dishes inside every course in every group
            # Determin and add to the list, the % of each dish in a specific course type for a specific group type
            distribution.append(len(persondict[persondf].loc[persondict[persondf][course] == dish]) / len(persondict[persondf][course]) * 100)
        labels = list(persondict[persondf][course].unique())      # Set labels and print the data distribution before begin plotting in the next part
        sizes = distribution
        print(course)
        print(persondf)
        print({i:j for i,j in zip(labels,distribution)})
    c += 1

"""
FOOD_FC
HealthyDF
{'SOUP': 100.0}
FOOD_SC
HealthyDF
{'SALAD': 100.0}
FOOD_TC
HealthyDF
{'ICE CREAM': 27.331189710610932, 'PIE': 72.66881028938906}
FOOD_FC
RetirementDF
{'TOMATO-MOZARELLA': 32.679978017952, 'OYSTERS': 66.99029126213593, 'SOUP': 0.3297307199120718}
FOOD_SC
RetirementDF
{'SPAGHETTI': 58.38065579776516, 'STEAK': 24.601575380106247, 'SALAD': 17.017768822128595}
FOOD_TC
RetirementDF
{'PIE': 89.70503181029497, 'ICE CREAM': 10.294968189705033}
FOOD_FC
BusinessDF
{'OYSTERS': 83.2238385734397, 'SOUP': 12.083528859690286, 'TOMATO-MOZARELLA': 4.692632566870014}
FOOD_SC
BusinessDF
{'LOBSTER': 100.0}
FOOD_TC
BusinessDF
{'PIE': 89.85915492957747, 'ICE CREAM': 10.140845070422536}
FOOD_FC
OnetimeDF
{'SOUP': 100.0}
FOOD_SC
OnetimeDF
{'SPAGHETTI': 72.03087885985748, 'STEAK': 27.969121140142516}
FOOD_TC
OnetimeDF
{'ICE CREAM': 9.626955475330927, 'PIE': 90.37304452466908}
"""

titles = ["FOOD_FC ,HealthyDF","FOOD_SC,HealthyDF","FOOD_TC ,HealthDF","FOOD_FC,RetirementDF","FOOD_SC,RetirementDF","FOOD_TC,RetirementDF",
          "FOOD_FC,BusinessDF","FOOD_SC,BusinessDF","FOOD_TC,BusinessDF","FOOD_FC,OnetimeDF","FOOD_SC,OnetimeDF","FOOD_TC,OnetimeDF"] # Set titles
data = [{'SOUP': 100.0},\
{'SALAD': 100.0},\
{'ICE CREAM': 27.331189710610932, 'PIE': 72.66881028938906},\
{'TOMATO-MOZARELLA': 32.679978017952, 'OYSTERS': 66.99029126213593, 'SOUP': 0.3297307199120718},\
{'SPAGHETTI': 58.38065579776516, 'STEAK': 24.601575380106247, 'SALAD': 17.017768822128595},\
{'PIE': 89.70503181029497, 'ICE CREAM': 10.294968189705033},\
{'OYSTERS': 83.2238385734397, 'SOUP': 12.083528859690286, 'TOMATO-MOZARELLA': 4.692632566870014},\
{'LOBSTER': 100.0},\
{'PIE': 89.85915492957747, 'ICE CREAM': 10.140845070422536},\
{'SOUP': 100.0},\
{'SPAGHETTI': 72.03087885985748, 'STEAK': 27.969121140142516},\
{'ICE CREAM': 9.626955475330927, 'PIE': 90.37304452466908},]    # Set percentages in dictionnaries for each food of each course of each client type

# Do the plotting part and have in total 12 graphs from 0 to 11 and properly plot the graphs with the correponding titles and details
for c in range(len(data)):
    elem = data[c]
    title = titles[c]
    labels = list(elem.keys())
    sizes = list(elem.values())
    explode = (0)*len(sizes)
    fig1, ax1 = plt.subplots()
    ax1.set_title(title)
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%',shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    plt.suptitle(title) # Set the title for the plot

    plt.savefig(os.path.abspath("../Result/part3-5-"+str(c)+".png"))  # Save the output in our project file
    plt.show()  # Show the plot
    plt.close()  # Close the figure just after you saving it to be able to save properly the upcoming figures


# Q6:
Drinks_Course1_Data = Drinks_Costs.loc[:,'DRINKS_FC'].values     # Get the courses data from columns names to ease the work with vectors
Drinks_Course2_Data = Drinks_Costs.loc[:,'DRINKS_SC'].values
Drinks_Course3_Data = Drinks_Costs.loc[:,'DRINKS_TC'].values

Drinks_Data = {'Drinks 1': {'data': Drinks_Course1_Data,     #  Create a dictionnary to make the plots with the corresponding details
                               'color': 'g',
                               'title': 'Drinks C1',
                               'row':0,
                               'column': 0},
                'Drinks 2': {'data': Drinks_Course2_Data,
                               'color': 'b',
                               'title': 'Drinks C2',
                               'row': 0,
                               'column': 1
                               },
                'Drinks 3': {'data': Drinks_Course3_Data,
                               'color': 'r',
                               'title': 'Drinks C3',
                               'row': 0,
                               'column': 2
                               },
                }

kwargs = dict(alpha=0.8, bins=10)     # Properties of colour and bar sizes for the different plots
fig, axes = plt.subplots(1, len(Drinks_Data), figsize=(20,9))     # Prepare the interface to plot properly our graphs

for key in Drinks_Data:     # Loop through the column names to plot for each course the distribution of costs with the necessary details
    axes[Drinks_Data[key]['column']].hist(Drinks_Data[key]['data'], **kwargs, color=Drinks_Data[key]['color'], label=Drinks_Data[key]['title'])
    axes[Drinks_Data[key]['column']].set_xlabel('Values')
    axes[Drinks_Data[key]['column']].set_ylabel('Frequency')
    axes[Drinks_Data[key]['column']].legend()

plt.suptitle('Drinks costs per course distribution histograms')     # Set the title for the plot

plt.savefig(os.path.abspath("../Result/part3-6.png"))    # Save the output in our project file
plt.show()     # Show the plot
plt.close()     # Close the figure just after you saving it to be able to save properly the upcoming figures


#------------------------------- PART_4 -------------------------------

import pandas as pd
import os
#BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(_file_)))
import random
import datetime as dt
import warnings
warnings.filterwarnings('ignore')
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        print(sqlite3.version)
        return conn
    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()
#the connection to the SQL makes the transformation of data and the simulation for the great population smoother

def create_table(dbfile):
    """
    Creates a table called simulation -> save the simulations data
    generated with Customer Class
    :param dbfile:
    :return: None
    """
    conn = sqlite3.connect(dbfile)
    conn.execute("""CREATE TABLE simulation (
                    TIME text,
                    CUSTOMERID text,
                    CUSTOMERTYPE text,
                    COURSE1 text,
                    COURSE2 text,
                    COURSE3 text,
                    DRINKS1 REAL,
                    DRINKS2 REAL,
                    DRINKS3 REAL,
                    TOTAL1 REAL,
                    TOTAL2 REAL,
                    TOTAL3 REAL,
                    DATE text
                    );""")



# conn = sqlite3.connect(dbfile)
# create_table(os.path.join('db', 'database.db'))

def insert_table(df, dbfile): #creat the function insert_table to

    tuples = list(df.itertuples(index=False, name=None))
    columns_list = df.columns.tolist()
    marks = ['?' for _ in columns_list]
    columns_list = f'({(",".join(columns_list))})'
    marks = f'({(",".join(marks))})'
    table_name = 'simulation'
    conn = sqlite3.connect(dbfile)
    conn.executemany(f'INSERT OR REPLACE INTO {table_name}{columns_list} VALUES {marks}', tuples)
    conn.commit()


class Customer:
    """
    Customer class
    Here we generate all the distributions (type of customer, dish, drink costs)
    This class will be called to generate each restaurant event
    """

    def _init_(self, raw_df):
        self.raw_df = raw_df
        self.fix_data_frame()
        self.client_types = self.raw_df['label_comparison'].unique()
        self.load_dists_food()
        self.load_dists_drink()
        self.defineClientType()
        self.load_dist_time()
        self.defineClientID()
        self.goToRestaurant()
        self.defineDataFrame()

    def defineDataFrame(self):
        """
        Generates the final output of the simulation
        dataframe with one row and all the information needed to include on the database, the simulation will go through client by client
        """

        prices = self.menu_prices()

        final_df = pd.DataFrame({
                                 'TIME': self.time,
                                 'CUSTOMERID': [self.client_id],
                                 'CUSTOMERTYPE': self.client_type,
                                 'COURSE1': self.dish_1,
                                 'COURSE2': self.dish_2,
                                 'COURSE3': self.dish_3,
                                 'DRINKS1': self.drink_1,
                                 'DRINKS2': self.drink_2,
                                 'DRINKS3': self.drink_3,
                                 'TOTAL1': prices[self.dish_1] + self.drink_1,
                                 'TOTAL2': prices[self.dish_2] + self.drink_2,
                                 'TOTAL3': prices[self.dish_3] + self.drink_3
                                    })

        self.final_df = final_df

    def goToRestaurant(self):
        """
        Go to restaurant event will serve as the function that will happen in the row of each, time, course and drink
        We use random.choices to simulate using the probability we have
        """

        self.dish_1 = random.choices(self.info_dist[self.client_type[0]]['dish_1']['Dish_1Course'],
                                     self.info_dist[self.client_type[0]]['dish_1']['percentage'])[0]

        self.dish_2 = random.choices(self.info_dist[self.client_type[0]]['dish_2']['Dish_2Course'],
                                     self.info_dist[self.client_type[0]]['dish_2']['percentage'])[0]

        self.dish_3 = random.choices(self.info_dist[self.client_type[0]]['dish_3']['Dish_3Course'],
                                     self.info_dist[self.client_type[0]]['dish_3']['percentage'])[0]

        self.drink_1_prob = random.choices(self.info_dist_drinks[self.client_type[0]]['drink_1']['course_1_drink'],
                                           self.info_dist_drinks[self.client_type[0]]['drink_1']['percentage'])[0]

        self.drink_2_prob = random.choices(self.info_dist_drinks[self.client_type[0]]['drink_2']['course_2_drink'],
                                           self.info_dist_drinks[self.client_type[0]]['drink_2']['percentage'])[0]

        self.drink_3_prob = random.choices(self.info_dist_drinks[self.client_type[0]]['drink_3']['course_3_drink'],
                                           self.info_dist_drinks[self.client_type[0]]['drink_3']['percentage'])[0]

        self.time = random.choices(self.time_dist_prob[self.client_type[0]]['TIME'].values,
                                    self.time_dist_prob[self.client_type[0]]['percentage'].values)[0]

        self.define_drink_price_starter()

        self.define_drink_price_main()

        self.define_drink_price_dessert()

    def define_drink_price_starter(self):
        """
        Method aiming to define the price of the drink on the starter
        We know the value of price of drink is unstable so we add the float value to it for the range that is the price of the food
        """
        if self.dish_1 == 'Soup':
            self.drink_1 = self.drink_1_prob * float(random.randrange(0, 8))

        elif self.dish_1 == 'Tomate Mozarella':
            self.drink_1 = self.drink_1_prob * float(random.randrange(0, 5))

        elif self.dish_1 == 'Oyster':
            self.drink_1 = self.drink_1_prob * float(random.randrange(0, 10))

        else:
            self.drink_1 = 0


    def define_drink_price_main(self):
        """
        Method aiming to define the price of the drink following the main course
        The same goes for main and desert like in starter
        """
        if self.dish_2 == 'Salad':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 11)

        elif self.dish_2 == 'Spaghetti':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 5)

        elif self.dish_2 == 'Steak':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 15)

        elif self.dish_2 == 'Lobster':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 15)

        else:
            self.drink_2 = 0

    def define_drink_price_dessert(self):
        """
        Method aiming to define the price of the drink following the dessert
        """

        if self.dish_3 == 'Ice Cream':
            self.drink_3 = self.drink_3_prob * random.randrange(0, 5)

        elif self.dish_3 == 'Pie':
            self.drink_3 = self.drink_2_prob * random.randrange(0, 5)

        else:
            self.drink_3 = 0



    @staticmethod
    def _returning_client_probs():
        """
        :return: The clients' probability of returning to the restaurant
        static method to bounds this function to this class to show probability of returning and prices of dish when prompt
        """
        probability_ret = {'Business': 0.5,
                           'Healthy': 0.7,
                           'Retirement': 0.9,
                           'Onetime': 0.0
                           }
        return probability_ret


    @staticmethod
    def menu_prices():
        """
        :return: Menu prices in a dict
        """
        d = {'Soup':  3,
             'Tomate Mozarella': 15,
             'Oyster': 20,
             'Salad': 9,
             'Spaghetti': 20,
             'Steak': 25,
             'Lobster': 40,
             'Ice cream': 15,
             'Pie': 10
        }

        return d

    def defineClientID(self):
        """
        Definies ClientID depeding if they are coming back or
        if they are new customers
        It depends on the probability of returning that we have defined above in order to get the new client ID or not
        """
        df = self.raw_df[['CLIENT_ID', 'label_comparison']]

        ret_prob = self._returning_client_probs()

        self.info_returning_client = {}

        for key in ret_prob:
            x = pd.DataFrame({'info': ['returning', 'no_returning'], 'prob': [ret_prob[key],
                                                                                        1-ret_prob[key]]})

            self.info_returning_client[key] = x

        returning_flag = random.choices(self.info_returning_client[self.client_type[0]]['info'].values,
                                    self.info_returning_client[self.client_type[0]]['prob'].values)[0]


        if returning_flag == 'no_returning': #if the id is new then we will simulate the new customerID, going through from top to bottom and adding one
            #this will lead to the simulation of all other values not in the original ID list
            conn = sqlite3.connect(os.path.join('db', 'database.db'))

            max_id = pd.read_sql_query("SELECT max(CUSTOMERID) as max FROM simulation", conn)['max'].values[0]

            if max_id == None:
                self.client_id = 'ID1'
            else:
                self.client_id = 'ID' + str(int(max_id.strip('ID')) + 1)
        else:
            get_id = random.randint(0, len(df[df['label_comparison'] == self.client_type[0]]))
            self.client_id = list(df[df['label_comparison'] == self.client_type[0]]['CLIENT_ID'])[get_id-1]



    def defineClientType(self):
        """
        Defines client type distribution
        """

        self.client_type = random.choices(self.total_dist['label_comparison'], self.total_dist['percentage'])



    def load_dist_time(self):
        """
        Defines the distribution time of each type of client

        """

        df = self.raw_df[['TIME', 'label_comparison']]

        df['count'] = 1

        self.time_dist = df.groupby(['TIME', 'label_comparison']).agg({'count': sum}).reset_index()

        self.time_dist_prob = {}

        for type in  self.time_dist['label_comparison'].unique():
            x = self.time_dist.loc[self.time_dist['label_comparison']==type]
            x['percentage'] = x['count']/x['count'].sum()
            self.time_dist_prob[type] = x


    def load_dists_food(self):
        """
        Defines the food distribution for each client type
        """

        df = self.raw_df[['Dish_1Course', 'Dish_2Course', 'Dish_3Course', 'label_comparison']]

        self.info_dist = {}

        df['count'] = 1

        melted_df = pd.melt(df, id_vars=['label_comparison', 'Dish_1Course',
                                         'Dish_2Course', 'Dish_3Course'], value_vars=['count'])

        self.total_dist = melted_df.groupby('label_comparison').agg({'value': sum}).reset_index()
        self.total_dist['percentage'] = self.total_dist['value'] / self.total_dist['value'].sum()

        for t in self.client_types:
            x = melted_df[melted_df['label_comparison'] == t]
            melted_df_dish_1 = x.groupby('Dish_1Course').agg({'value': sum}).reset_index()
            melted_df_dish_1['percentage'] = melted_df_dish_1['value'] / melted_df_dish_1['value'].sum()
            melted_df_dish_2 = melted_df.groupby('Dish_2Course').agg({'value': sum}).reset_index()
            melted_df_dish_2['percentage'] = melted_df_dish_2['value'] / melted_df_dish_2['value'].sum()
            melted_df_dish_3 = melted_df.groupby('Dish_3Course').agg({'value': sum}).reset_index()
            melted_df_dish_3['percentage'] = melted_df_dish_3['value'] / melted_df_dish_3['value'].sum()

            self.info_dist[t] = {
                'dish_1': melted_df_dish_1,
                'dish_2': melted_df_dish_2,
                'dish_3': melted_df_dish_3,
            }

    def load_dists_drink(self):
        """
        Defining the drink distribution for each client type
        """
        df = self.raw_df[[ 'course_1_drink', 'course_2_drink', 'course_3_drink', 'label_comparison']]
        self.info_dist_drinks = {}

        df['count'] = 1

        melted_df = pd.melt(df, id_vars=['label_comparison', 'course_1_drink',
                                         'course_2_drink', 'course_3_drink'], value_vars=['count'])

        self.total_dist = melted_df.groupby('label_comparison').agg({'value': sum}).reset_index()
        self.total_dist['percentage'] = self.total_dist['value'] / self.total_dist['value'].sum()

        for t in self.client_types:
            x = melted_df[melted_df['label_comparison'] == t]
            melted_df_dish_1 = x.groupby('course_1_drink').agg({'value': sum}).reset_index()
            melted_df_dish_1['percentage'] = melted_df_dish_1['value'] / melted_df_dish_1['value'].sum()
            if len(melted_df_dish_1.loc[melted_df_dish_1['course_1_drink'] == 0]['percentage']) > 0:
                percentage_1_drink = melted_df_dish_1.loc[melted_df_dish_1['course_1_drink'] == 0]['percentage'].values[
                    0]
            else:
                percentage_1_drink = 1

            melted_df_dish_2 = melted_df.groupby('course_2_drink').agg({'value': sum}).reset_index()
            melted_df_dish_2['percentage'] = melted_df_dish_2['value'] / melted_df_dish_2['value'].sum()

            if len(melted_df_dish_2.loc[melted_df_dish_2['course_2_drink'] == 0]['percentage']) > 0:
                percentage_2_drink = melted_df_dish_2.loc[melted_df_dish_2['course_2_drink'] == 0]['percentage'].values[
                    0]
            else:
                percentage_2_drink = 1

            melted_df_dish_3 = melted_df.groupby('course_3_drink').agg({'value': sum}).reset_index()
            melted_df_dish_3['percentage'] = melted_df_dish_3['value'] / melted_df_dish_3['value'].sum()

            if len(melted_df_dish_3.loc[melted_df_dish_3['course_3_drink'] == 0]['percentage']) > 0:
                percentage_3_drink = melted_df_dish_3.loc[melted_df_dish_3['course_3_drink'] == 0]['percentage'].values[
                    0]
            else:
                percentage_3_drink = 1

            self.info_dist_drinks[t] = {
                'drink_1': pd.DataFrame(
                    {'course_1_drink': [0, 1], 'percentage': [percentage_1_drink, 1 - percentage_1_drink]}),
                'drink_2': pd.DataFrame(
                    {'course_2_drink': [0, 1], 'percentage': [percentage_2_drink, 1 - percentage_2_drink]}),
                'drink_3': pd.DataFrame(
                    {'course_3_drink': [0, 1], 'percentage': [percentage_3_drink, 1 - percentage_3_drink]}),
            }

if _name_ == '_main_':

    conn = sqlite3.connect(os.path.join('db', 'database.db'))

    last_day = pd.read_sql_query("SELECT max(DATE) as last_date FROM simulation", conn)['last_date'].values[0]

    df_today = pd.read_sql_query(f"SELECT * FROM simulation WHERE DATE = '{last_day}'", conn)

    today = pd.read_sql_query("SELECT max(DATE) as last_date FROM simulation", conn)['last_date'].values[0]

    init_date = dt.datetime.strptime(today, '%Y-%m-%d') + dt.timedelta(1)
    end_date = dt.datetime.strptime(today, '%Y-%m-%d') + dt.timedelta(2)

    range_dates = pd.date_range(init_date, end_date)

    raw_df = pd.read_excel(os.path.join('Results', 'Part3', 'Courses_With_Label.xlsx'))

    #
    for day in range_dates:


        for i in range(20):

            print(i, day)

            x = Customer(raw_df)

            result = x.final_df

            result['DATE'] = day.strftime("%Y-%m-%d")

            insert_table(result, os.path.join('db', 'database.db'))

